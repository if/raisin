%{
#include "raisin.tab.h"
#include "ast.h"

union nodeval yylval;
%}

%option noyywrap

%%


"func" { return   FUNC; }
"{"    { return LBRACE; }
"}"    { return RBRACE; }

[0-9]+ {
        yylval.num = atoi(yytext);
        return NUM;
}

[-a-z][-a-z0-9]* {
        yylval.str = strdup(yytext);
        return IDENT;
}

"::"[-a-z][-a-z0-9]* {
        yylval.str = strdup(yytext+2);
        return SYM;
}

#.*$                            /* ignore */
[ \t\n]+                        /* ignore */
