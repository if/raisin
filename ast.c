#include <stdio.h>
#include <stdlib.h>
#include "ast.h"


/* alloc a new node and initialize its type. */
struct node
*mknode(nodetype nty)
{
        struct node *n = malloc(sizeof(*n));
        n->nty = nty;
        return n;
}

/* alloc a new func node */
struct node
*ndfunc(char *name, struct node *ret)
{
        struct node *n = mknode(N_FUNC);
        n->val.f = malloc(sizeof(struct func));
        n->val.f->name = name;
        n->val.f->ret  =  ret;
        return n;
}
