#ifndef __RAISIN_H__
#define __RAISIN_H__

#include "ast.h"

void compile_ast(struct node *root, char *module_name);

#endif
