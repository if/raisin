#ifndef __ASM_H__
#define __ASM_H__

#include "module.h"


/* number of pre-initialized chunk entries */
#define INIT_ATOMS 4
#define INIT_EXPTS 2
#define INIT_IMPTS 2
/* number of bytes added to finalized code */
#define FINAL_BYTES 40


void asm_module(FILE *fp, struct module *m);


#endif
