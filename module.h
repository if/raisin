#ifndef __MODULE_H__
#define __MODULE_H__

#include "hash_tab.h"


struct bytecode {
        uint8_t *bytes;
        int len;
        int capacity;
        int lbl_count;
        int func_count;
};


struct expt_info {
        uint32_t name;
        uint32_t arity;
        uint32_t lbl;
};

struct expt_tab {
        struct expt_info *expts;
        uint32_t count;
};


struct impt_info {
        uint32_t module;
        uint32_t name;
        uint32_t arity;
};

struct impt_tab {
        struct impt_info *impts;
        int        count;
};


struct module {
        char *name;
        hash_tab *syms;
        hash_tab *strs;
        struct expt_tab *expt;
        struct impt_tab *impt;
        struct bytecode *code;
};


struct module *mkmodule(char *name);
int add_atom(struct module *module, char *atom);
int add_expt(struct module *module, int   name);
int  add_str(struct module *module, char *str, uint32_t len);
void finalize_code(struct module *m);

char **module_syms(struct module *m, uint32_t *len);
char **module_strs(struct module *m, uint32_t *len);

/* TODO: add_<instruction> */

#endif
