#include <string.h>
#include "common.h"
#include "hash_tab.h"

#define TAB_MAX_LOAD 0.75

#define ALLOC(type, count) \
    (type*)reallocate(NULL, 0, sizeof(type) * (count))

#define GROW_CAPACITY(capacity) \
    ((capacity < 8)? 8 : (capacity) * 2)

#define GROW_ARRAY(ty, ptr, oldc, newc) \
    (ty*)reallocate(ptr, sizeof(ty) * (oldc), \
                    sizeof(ty) * (newc))

#define FREE(type, ptr) reallocate(ptr, sizeof(type), 0)

#define FREE_ARRAY(ty, ptr, oldc) \
    reallocate(ptr, sizeof(ty) * (oldc), 0)


static void
*reallocate(void *ptr, size_t old_size, size_t new_size)
{
        if (new_size == 0) {
                free(ptr);
                return NULL;
        }

        void *result = realloc(ptr, new_size);
        if (result == NULL) exit(1);
        return result;
}




void
hash_tab_init(hash_tab *tab)
{
        tab->count    = 0;
        tab->capacity = 0;
        tab->entries  = NULL;
}

void
hash_tab_free(hash_tab *tab)
{
        FREE_ARRAY(entry, tab->entries, tab->capacity);
        hash_tab_init(tab);
}


/* the FNV-1a hash function (see "Crafting Interpreters", ch 20.3) */
static uint32_t
hash_str(char *key/* , int len */)
{
        int len = strlen(key);
        uint32_t hash = 2166136261u;

        for (int i = 0; i < len; i++) {
                hash ^= (uint8_t)key[i];
                hash *= 16777619;
        }

        return hash;
}


/* find the appropriate bucket for a key, using linear probing */
static entry
*find_entry(entry *entries, int capacity, char *key) 
{
        uint32_t index = hash_str(key) % capacity;
        /* the last tombstone entry visited */
        entry *tombstone = NULL;

        /* load factor limit prevents this from being an infinite loop */
        while (true) {
                entry *entry = &entries[index];

                if (entry->key == NULL) {
                        /* empty entry (reuse last tombstone) */
                        if (entry->value == NULL) {
                                return (tombstone != NULL ? tombstone : entry);
                        } else {
                                /* tombstone */
                                if (tombstone == NULL)
                                        tombstone = entry;
                        }
                } else if (entry->key == key) {
                        /* return the entry if we found its key */
                        return entry;
                }

                /* otherwise, look at next entry */
                index = (index+1) % capacity;
        }
}


static void
adjust_capacity(hash_tab *tab, int new_capacity) 
{
        entry *entries = ALLOC(entry, new_capacity);

        for (int i = 0; i < new_capacity; i++) {
                entries[i].key   = NULL;
                entries[i].value = NULL;
        }

  
        /* put entries in new array (indices will be different) */
        /* also reset the count since the tombstones aren't copied */
        tab->count = 0;
        for (int i = 0; i < tab->capacity; i++) {
                entry *_entry = &tab->entries[i];
                if (_entry->key == NULL) continue;

                entry *dest = find_entry(entries, new_capacity, _entry->key);
                dest->key = _entry->key;
                dest->value = _entry->value;
                tab->count++;
        }
  
        /* free old entries array */
        FREE_ARRAY(entry, tab->entries, tab->capacity);

        tab->entries = entries;
        tab->capacity = new_capacity;
}


/* add a k-v pair to the table; returns true if this
   overwrites an existing k-v' pair */
bool
hash_tab_set(hash_tab *tab, char *key, void *val)
{
        /* grow the table if load factor gets too high */
        if ((tab->count + 1) > (tab->capacity * TAB_MAX_LOAD)) {
                int new_capacity = GROW_CAPACITY(tab->capacity);
                adjust_capacity(tab, new_capacity);
        }


        entry *entry = find_entry(tab->entries, tab->capacity, key);
        bool is_new_key = (entry->key == NULL);
        /* increment count if inserting into empty bucket 
           (i.e. not reusing a tombstone bucket) */
        if (is_new_key && entry->value == NULL)
                tab->count++;
        
        entry->key = key;
        entry->value = val;
        
        return is_new_key;
}



/* returns `true' and sets `val' if `key' is found */
bool
has_tab_get(hash_tab *tab, char *key, void **val)
{
        if (tab->count == 0) return false;
        
        entry *entry = find_entry(tab->entries, tab->capacity, key);
        if (entry->key == NULL) return false;
        
        *val = entry->value;
        return true;
}


bool
hash_tab_remove(hash_tab *tab, char *key)
{
        if (tab->count == 0) return false;

        entry *entry = find_entry(tab->entries, tab->capacity, key);
        if (entry->key == NULL) return false;
        
        /* place a "tombstone" at the entry, to preserve the linear
         * probing chains */
        entry->key = NULL;
        /* entry->value = BOOL_VAL(true); */
        entry->value = NULL;
        return true;
}




/* for debugging -- probably kind of broken */
void
hash_tab_print(hash_tab *tab)
{
        printf("count: %d; cap: %d\n", tab->count, tab->capacity);

        for (int i = 0; i < tab->capacity; i++) {
                entry e = tab->entries[i];
                if (e.key != NULL) {
                        printf("[%d] %s\n", i, e.key);
                }
        }
}
