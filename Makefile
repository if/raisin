CC = gcc
CFLAGS = -Wall -g
LFLAGS = -lfl
EXEC = raisin
SOURCES = main.c       \
          lex.yy.c     \
	      raisin.tab.c \
          ast.c        \
          asm.c        \
          hash_tab.c   \
	      module.c
OBJS = *.o

all:
	make clean
	make $(EXEC)


$(EXEC): $(SOURCES)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@

lex.yy.c: raisin.y raisin.l
	bison -t -d raisin.y
	flex raisin.l


clean:
	rm -vf $(EXEC) $(OBJS) raisin.tab.h raisin.tab.c lex.yy.c
