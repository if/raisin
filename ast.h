#ifndef __AST_H__
#define __AST_H__

struct node;
struct func;
union nodeval;

typedef enum {
        N_NUM,
        N_SYM,
        N_IDENT,
        N_FUNC,
} nodetype;

union nodeval {
        char *str;
        struct node *nd;
        struct func  *f;
        int num;
};

struct func {
        char *name;
        struct node *ret;
};


struct node {
        union nodeval val;
        nodetype nty;
}; 



struct node *mknode(nodetype nty);
struct node *ndfunc(char *name, struct node *ret);


#endif
