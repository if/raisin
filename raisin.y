%{
        #include <stdio.h>
        #include <stdlib.h>
        #include <string.h>
        #include <libgen.h>
        #include <assert.h>
        #include "ast.h"
        #include "raisin.h"

        extern int yylex();                
        extern int yyparse();
        void yyerror(const char *s);

        extern FILE *yyin; 
        struct node *root = 0;
%}

%define api.value.type {union nodeval}

%token FUNC LBRACE RBRACE
%token <num>   NUM;
%token <str> IDENT;
%token <str>    SYM;

%type <nd> func val;

%%

/* input: %empty | input line */

/* line: '\n' | exp '\n' */

/* exp: val */

/* for now, a simple function that returns a value */
/* func: "func" IDENT '{' val '}' { puts("func!!!"); }; */
func: FUNC IDENT[N] LBRACE val[R] RBRACE {
        struct node *n = ndfunc($N, $R);
        root = n;
        $$ = n;
};


val:
  NUM   { struct node *n = mknode(N_NUM);
          n->val.num = $1; yylval.nd = n; $$ = n; }
/* | IDENT { struct node *n = mknode(N_IDENT); */
/*           n->val.str = $1; yylval.nd = n; $$ = n; } */
| SYM   { struct node *n = mknode(N_SYM);
          n->val.str = $1; yylval.nd = n; $$ = n; }
;


%%

void
yyerror(const char *s)
{       
        printf("parse error on input: %s\n", s);
        printf("goodbye.\n");
        exit(-1);
}

void
parse_args(int argc, char **argv, FILE **in, char **module_name)
{
        if (argc != 2) {
                fprintf(stderr, "expected exactly one argument.\n");
                fprintf(stderr, "USAGE: raisin MODULE.ext\n");
                fprintf(stderr, "\t(outputs MODULE.beam)\n");
                exit(1);
        }

        /* extract module name from file path */
        /* first, get the base name */
        char *modc, *mod;
        char *input_path = argv[1];
        modc = strdup(input_path);
        mod  = basename(modc);
        /* get module name by removing extension */
        char *ext = strrchr(mod, '.');
        if (ext != NULL) ext[0] = '\0'; /* kludge??? */
        *module_name = mod;


        FILE *inf = fopen(input_path, "r");
        if (inf == NULL) {
                fprintf(stderr, "could not open file %s for reading\n",
                        input_path);
                exit(1);
        }

        *in = inf;
}

int
main(int argc, char **argv)
{
        FILE *in = stdin;
        char *module_name;
        parse_args(argc, argv, &in, &module_name);

        yyin = in;
        yyparse();

        compile_ast(root, module_name);

        return 0;
}




/* ---sample of prototype raisin syntax---
   # naïve approach
   func greet name {
       greeting <- cat "hello, " name
       puts greeting
   }

   # erlang approach
   func greet name {
       io/format "hello, ~a" [name]
   }

   # optional approach
   func switch greet name {
       ;nothing -> "hello, world"
       ;just  s -> cat "hello, " s
   }

   # optional approach, with sugar
   :: greet (opt str -> unit)
   func greet (name="world") {
       io/format "hello, ~a" [name]
   }

   # mailbox stuff
   :: server (server-ctl | unit)
   func server {
       while {m <- mail, loop mail}
   }

   :: loop (server-ctl -> bool)
   func switch loop ctl {
       ;print thing -> {io/fmt thing, ;true}
       ;stop -> unit
   }
 */
