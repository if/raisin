#include <string.h>
#include "common.h"
#include "raisin.h"
#include "module.h"
#include "ast.h"
#include "asm.h"



void
compile_ast(struct node *root, char *module_name)
{
        FILE *fpout = stdout;
        struct module *m = mkmodule(module_name);

        finalize_code(m);
        asm_module(fpout, m);
}
