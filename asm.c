/* BEAM bytecode assembler */
#include <string.h>
#include "common.h"
#include "asm.h"

#define BEAM_ALIGN 4
#define EXPT_ENTRY_SIZE 12
#define IMPT_ENTRY_SIZE 12
#define CODE_SUB_SIZE 16
#define INSTR_SET 0
#define OPCODE_MAX 0x99
#define CHUNK_START 8               /* tag + size */
#define TAB_START (CHUNK_START + 4) /* tag + size + count */

#define IFF_HDR  "FOR1"
#define BEAM_HDR "BEAM"
#define EXPT_HDR "ExpT"
#define IMPT_HDR "ImpT"
#define ATOM_HDR "AtU8"
#define STR_HDR  "StrT"
#define CODE_HDR "Code"



/* align a number `n' to the next multiple of `amt' */
uint32_t
align(uint32_t amt, uint32_t n)
{
        int aligned = amt * ((n + amt - 1) / amt);
        return aligned;
}


/* ===== SIZE FUNCTIONS  */
/* note that size functions return the entire size of a chunk,
   but the `size' parameter within a chunk only measures the
   size of the bytes that come after it; this means that the
   size parameter ends up 8 bytes less than the chunk's entire
   size, because it doesn't account for itself (4 bytes) and the
   chunk tag (4 bytes). */

uint32_t
size_expt(struct expt_tab *tab, uint32_t *pad)
{
        /* tab header + tab size + tab count + entries */
        uint32_t raw_size = TAB_START + (EXPT_ENTRY_SIZE * tab->count);
        uint32_t size = align(BEAM_ALIGN, raw_size);
        uint32_t padding = size - raw_size;

        if (pad != NULL) *pad = padding;

        return size;
}

uint32_t
size_atom_tab(char **atoms, int len, uint32_t *pad)
{
        /* tab header + tab size + tab count */
        uint32_t raw_size = TAB_START;

        for (int i = 0; i < len; i++)
                raw_size += (1 + strlen(atoms[i]));

        uint32_t size = align(BEAM_ALIGN, raw_size);
        uint32_t padding = size - raw_size;
        
        if (pad != NULL) *pad = padding;

        return size;
}

uint32_t
size_str_tab(char **strs, int len, uint32_t *pad)
{
        uint32_t raw_size = CHUNK_START;

        for (int i = 0; i < len; i++)
                raw_size += 1 + strlen(strs[i]);
        
        uint32_t size = align(BEAM_ALIGN, raw_size);
        uint32_t padding = size - raw_size;

        if (pad != NULL) *pad = padding;

        return size;
}


uint32_t
size_impt(struct impt_tab *tab, uint32_t *pad)
{
        uint32_t raw_size = TAB_START + (IMPT_ENTRY_SIZE * tab->count);
        uint32_t size = align(BEAM_ALIGN, raw_size);
        uint32_t padding = size - raw_size;

        if (pad != NULL) *pad = padding;

        return size;
}

uint32_t
size_code(struct bytecode *code, uint32_t *pad)
{
        /* ((header + size) + sub_size) + <sub_size>  */
        uint32_t raw_size = CHUNK_START + 4 + CODE_SUB_SIZE;
        raw_size += code->len;   /* simple as */
        uint32_t size = align(BEAM_ALIGN, raw_size);
        uint32_t padding = size - raw_size;
        
        if (pad != NULL) *pad = padding;

        return size;
}





/* print `pad' number of 0x00 bytes */
void
fprintpad(FILE *fp, uint32_t pad)
{
        for (int i = 0; i < pad; i++)
                fputc(0x00, fp);
}

void
fprintu32(FILE *fp, uint32_t n)
{
        for (int i = 0; i < sizeof n; i++) {
                uint32_t sh = i * 8;
                unsigned char c = n << sh >> 24;
                fputc(c, fp);
        }
}


/* assemble a BEAM header chunk */
void
asm_beam_hdr(FILE *fp, uint32_t size)
{
        fprintf(fp, IFF_HDR);
        fprintu32(fp, size);
        fprintf(fp, BEAM_HDR);
}

/* assemble an entry in an export chunk (func info) */
void
asm_expt_entry(FILE *fp, struct expt_info e)
{
        fprintu32(fp, e.name);
        fprintu32(fp, e.arity);
        fprintu32(fp, e.lbl);
}

/* assemble an export chunk */
void 
asm_expt(FILE *fp, struct expt_tab *tab,
         uint32_t size, uint32_t padding)
{
        size -= CHUNK_START;

        /* print header and size */
        fprintf(fp, EXPT_HDR);
        fprintu32(fp, size);
        fprintu32(fp, tab->count);
        /* print functions in expt table */
        struct expt_info *entries = tab->expts;
        for (int i = 0; i < tab->count; i++)
                asm_expt_entry(fp, entries[i]);

        /* print any padding bytes at end */
        fprintpad(fp, padding);
}

void
asm_impt_entry(FILE *fp, struct impt_info e)
{
        fprintu32(fp, e.module);
        fprintu32(fp, e.name);
        fprintu32(fp, e.arity);
}


void
asm_impt(FILE *fp, struct impt_tab *tab,
         uint32_t size, uint32_t padding)
{
        size -= CHUNK_START;
        
        fprintf(fp, IMPT_HDR);
        fprintu32(fp, size);
        fprintu32(fp, tab->count);
        /* print imported functions */
        struct impt_info *entries = tab->impts;
        for (int i = 0; i < tab->count; i++)
                asm_impt_entry(fp, entries[i]);

        fprintpad(fp, padding);
}


void
asm_atom_entry(FILE *fp, char *a)
{
        size_t len = strlen(a);

        fputc((unsigned char)len, fp);
        for (size_t i = 0; i < len; i++)
                fputc((unsigned char)a[i], fp);
}

void
asm_atom_tab(FILE *fp, char **atoms, int len,
             uint32_t size, uint32_t padding)
{
        size -= CHUNK_START;

        fprintf(fp, ATOM_HDR);
        fprintu32(fp, size);
        fprintu32(fp, len);

        /* print atoms in table */
        for (int i = 0; i < len; i++)
                asm_atom_entry(fp, atoms[i]);

        fprintpad(fp, padding);
}

void
asm_str_tab(FILE *fp, char **strs, int len,
            uint32_t size, uint32_t padding)
{
        size -= CHUNK_START;

        fprintf(fp, STR_HDR);
        fprintu32(fp, size);
        /* strings ... */
        fprintpad(fp, padding);
}



void
asm_code(FILE *fp, struct bytecode *code,
         uint32_t size, uint32_t padding)
{
        size -= CHUNK_START;

        fprintf(fp, CODE_HDR);
        fprintu32(fp, size);
        fprintu32(fp, CODE_SUB_SIZE);
        fprintu32(fp, INSTR_SET);
        fprintu32(fp, OPCODE_MAX);
        fprintu32(fp, code->lbl_count);
        fprintu32(fp, code->func_count);

        /* print code */
        for (int i = 0; i < code->len; i++)
                fputc(code->bytes[i], fp);

        fprintpad(fp, padding);
}




/*************************************************************/


void
asm_module(FILE *fp, struct module *m)
{
        /* a little messy, admittedly */
        uint32_t atoms_len, strs_len;
        char **atoms = module_syms(m, &atoms_len);
        char **strs = module_strs(m, &strs_len);
        
        uint32_t size = 4;      /* start of BEAM hdr */
        uint32_t atom_pad, str_pad, code_pad, impt_pad, expt_pad;
        uint32_t atom_size, str_size, code_size, impt_size, expt_size;
        
        size += (atom_size = size_atom_tab(atoms, atoms_len, &atom_pad));
        size += (str_size  = size_str_tab(strs, strs_len, &str_pad));
        size += (code_size = size_code(m->code, &code_pad));
        size += (impt_size = size_impt(m->impt, &impt_pad));
        size += (expt_size = size_expt(m->expt, &expt_pad));

        asm_beam_hdr(fp, size);
        asm_atom_tab(fp, atoms, atoms_len, atom_size, atom_pad);
        asm_code(fp, m->code, code_size, code_pad);
        asm_str_tab(fp, strs, strs_len, str_size, str_pad);
        asm_impt(fp, m->impt, impt_size, impt_pad);
        asm_expt(fp, m->expt, expt_size, expt_pad);
}
