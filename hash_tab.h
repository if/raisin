#ifndef __HASH_TAB_H__
#define __HASH_TAB_H__

#include "common.h"


typedef struct {
        char *key;
        void *value;
} entry;


typedef struct {
        int count;
        int capacity;
        entry *entries;
} hash_tab;



void hash_tab_init(hash_tab *tab);
void hash_tab_delete(hash_tab *tab);
bool hash_tab_set(hash_tab *tab, char *key, void  *val);
bool hash_tab_get(hash_tab *tab, char *key, void **val);
bool hash_tab_remove(hash_tab *tab, char *key);

void hash_tab_print(hash_tab *tab);


#endif
