#include "common.h"
#include "module.h"


/* convert a hash table (whose values are indices) into a list,
   with (tab[k] = i) --> (list[i] = k). */
char
**tab2list(hash_tab *tab)
{
        char **list = calloc(tab->count, sizeof(char *));

        for (int i = 0; i < tab->capacity; i++) {
                entry e = tab->entries[i];
                if (e.key == NULL) continue;
                list[(uint64_t)e.value] = e.key;
        }

        return list;
}




struct expt_tab
*init_expt()
{
        struct expt_tab *expt = malloc(sizeof (*expt));
        expt->expts = calloc(2, sizeof(struct expt_info));

        /* [label 4] module_info/1 */
        /* [label 2] module_info/0 */
        expt->count = 2;
        expt->expts[0] = (struct expt_info){2, 1, 4};
        expt->expts[1] = (struct expt_info){2, 0, 2};

        return expt;
}

struct impt_tab
*init_impt()
{
        struct impt_tab *impt = malloc(sizeof (*impt));
        impt->impts = calloc(2, sizeof(struct impt_info));

        /* erlang:get_module_info/1 */
        /* erlang:get_module_info/2 */
        impt->count = 2;
        impt->impts[0] = (struct impt_info){3, 4, 1};
        impt->impts[1] = (struct impt_info){3, 4, 2};

        return impt;
}


hash_tab
*init_syms(char *modname)
{
        hash_tab *tab = malloc(sizeof(*tab));
        hash_tab_init(tab);
        
        hash_tab_set(tab, modname,           (void *)0);
        hash_tab_set(tab, "module_info",     (void *)1);
        hash_tab_set(tab, "erlang",          (void *)2);
        hash_tab_set(tab, "get_module_info", (void *)3);

        return tab;
}


struct bytecode
*init_bytecode(int hint)
{
        struct bytecode *code = malloc(sizeof(*code));
        code->bytes = calloc(hint, sizeof(uint8_t));
        code->len = 0;
        code->capacity = hint;

        return code;
}



char
**module_syms(struct module *m, uint32_t *len)
{
        *len = m->syms->count;
        return tab2list(m->syms);
}

char
**module_strs(struct module *m, uint32_t *len)
{
        *len = m->strs->count;
        return tab2list(m->strs);
}



struct module
*mkmodule(char *name)
{
        struct module *m = malloc(sizeof (*m));
        
        m->name = name;
        m->syms = init_syms(name);

        m->strs = malloc(sizeof (*m->strs));
        hash_tab_init(m->strs);

        m->expt = init_expt();
        m->impt = init_impt();

        /* make room for module_info defns at end of code */
        m->code = init_bytecode(40);
        m->code->lbl_count  = 5;
        m->code->func_count = 2;

        return m;
}


/* Add code for module_info/0 and module_info/1 at the
   very end of the module's code chunk.  */
void
finalize_code(struct module *m)
{
        /* grow code to fit 40 new bytes */
        int free_space = m->code->capacity - m->code->len;
        int new_space = MAX(0, 40 - free_space);
        size_t new_size = sizeof(uint8_t) * (m->code->capacity + new_space);
        m->code->bytes = (uint8_t *)realloc(m->code->bytes, new_size);

        /* add in the two functions */
        uint8_t final_code[40] = {
                /* ===== module_info/0 ===== */
                0x01, 0x10,             /* label 1 */
                0x99, 0x00,             /* line info */
                0x02, 0x12, 0x22, 0x00, /* func_info MODULE:module_info/0 */
                0x01, 0x20,             /* label 2 */
                0x40, 0x12, 0x03,       /* move MODULE x0 */
                0x99, 0x00,             /* line info */
                0x4e, 0x10, 0x00,       /* call_ext_only ... */
                /* ===== module_info/1 ===== */
                0x01, 0x30,             /* label 3 */
                0x99, 0x00,             /* line info */
                0x02, 0x12, 0x22, 0x10, /* func_info MODULE:module_info/1 */
                0x01, 0x40,             /* label 4 */
                0x40, 0x03, 0x13,       /* move x0 x1 */
                0x40, 0x12, 0x03,       /* move MODULE x0 */
                0x99, 0x00,             /* line info */
                0x4e, 0x20, 0x10,       /* call_ext_only ... */
                0x03,                   /* ??? */
        };

        for (int i = 0; i < 40; i++)
                m->code->bytes[m->code->len++] = final_code[i];

        m->code->capacity += new_space;
}
